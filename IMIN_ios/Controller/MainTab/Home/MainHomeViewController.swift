//
//  MainHomeViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/10.
//

// File : MainHomeViewController.swift
// Menu : 메인 홈 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.10)

import UIKit
import Foundation

class MainHomeViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var scrollViewContainer: UIScrollView!
    @IBOutlet weak var verticalStackView: UIStackView!
    @IBOutlet weak var constraintTopViewHeight: NSLayoutConstraint!
    
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 리프레쉬 컨트롤
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.addTarget(self, action: #selector(self.refreshExerciseList(_:)), for: .valueChanged)
        scrollViewContainer.refreshControl = self.refreshControl

        self.constraintTopViewHeight.constant = 0
    }
    
    @objc private func refreshExerciseList(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()

                UIView.animate(withDuration: 0.4, animations: {
                    self.constraintTopViewHeight.constant = 213
                    self.view.layoutIfNeeded()
                }) { (complete) in

                }
                
                // 2.5초후 탑뷰 height = 0
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    UIView.animate(withDuration: 0.4, animations: {
                        self.constraintTopViewHeight.constant = 0
                        self.view.layoutIfNeeded()
                    }) { (complete) in

                    }
                }
            }
        }
    }
}
