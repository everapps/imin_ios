//
//  MainTabViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/02.
//

import UIKit
import Foundation

class MainTabViewController: UITabBarController, UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("item.title = \(item.title!)")
        
        if item.title! == "홈" {
        } else if item.title! == "재무건강" {
        } else if item.title! == "MY 금융" {
        }
    }
}
