//
//  IntroViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/02.
//

import UIKit
import Foundation
import Lottie

class IntroViewController: UIViewController {
    @IBOutlet weak var lottieContainerView: UIView!
    
    var animationView2: AnimationView!
    var animName2: String = "img_splash_imin_logo_ios"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        animationView2 = AnimationView(name: animName2) // AnimationView(name: "lottie json 파일 이름")으로 애니메이션 뷰 생성
        animationView2.frame = CGRect(x: 0, y: 0, width: lottieContainerView.frame.size.width, height: lottieContainerView.frame.size.height) // 애니메이션뷰의 크기 설정
        animationView2.contentMode = .scaleAspectFit // 애니메이션뷰의 콘텐트모드 설정
        lottieContainerView.addSubview(self.animationView2) // 애니메이션뷰를 컨테이너뷰에 추가
        playIntroAnim2()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            let isShowTutoral = DataManager.shared().appSetting.isShowTutoral
            if isShowTutoral == "Y" {
                //UIManager.shared().showMainTab()
                UIManager.shared().showMainLogin()
            } else {
                UIManager.shared().showTutorial()
            }

            /*
            let password = DataManager.shared().appSetting.password
            if password != nil && password != "" {
                UIManager.shared().showMainTab()

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    // everapps. 테스트.
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: UIDef.ShowPasswordVerifyNotificatoin), object: nil)
                }

            } else {
                UIManager.shared().showIntroSwipe()
            }
            */
        }
    }
    
    func playIntroAnim2() {
        //self.animationView.play() // 애미메이션뷰 실행
        self.animationView2.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.playOnce) { (finished) in
            if finished == true {
                self.animationView2.currentProgress = 0.9
            }
        }
    }
}
