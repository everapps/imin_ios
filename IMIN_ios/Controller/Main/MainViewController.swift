//
//  MainViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/02.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet weak var contentContainerView : UIView!
    var contentViewController : UINavigationController!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }


    // MARK: ContentView Manage
    
    func setMainContentViewController(_ contentViewController: UINavigationController) {
        addChild(contentViewController)
        self.contentContainerView.addSubview(contentViewController.view)
        contentViewController.didMove(toParent: self)
        contentViewController.view.frame = CGRect(x: 0, y: 0, width: self.contentContainerView.frame.size.width, height: self.contentContainerView.frame.size.height)
        contentViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentViewController.view.alpha = 0.0
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions(), animations: {
            contentViewController.view.alpha = 1.0
            
        }, completion: { (finished: Bool) in
            if (nil != self.contentViewController) {
                self.contentViewController.willMove(toParent: nil)
                self.contentViewController.view.removeFromSuperview()
                self.contentViewController.removeFromParent()
            }
            self.contentViewController = contentViewController
        })
    }
}

