//
//  SignUpMoreInfoViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/08.
//

// File : SignUpMoreInfoViewController.swift
// Menu : 회원가입시 추가정보 입력 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.8)

import UIKit
import Foundation

class SignUpMoreInfoViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldBirthday: UITextField!
    @IBOutlet weak var textFieldPhoneNum: UITextField!
    @IBOutlet weak var textFieldNickName: UITextField!
    @IBOutlet weak var textFieldAddress: UITextField!
    @IBOutlet weak var textFieldAddressDetail: UITextField!
    @IBOutlet weak var textFieldRecommandCode: UITextField!

    @IBOutlet weak var viewContainerBirthdayError: UIView!
    @IBOutlet weak var viewContainerPhoneNumError: UIView!
    @IBOutlet weak var viewContainerNickNameError: UIView!
    @IBOutlet weak var viewContainerRecommCodeError: UIView!
    @IBOutlet weak var lblNickNameErrorText: UILabel!

    @IBOutlet weak var lineName: UIView!
    @IBOutlet weak var lineBirthday: UIView!
    @IBOutlet weak var linePhoneNum: UIView!
    @IBOutlet weak var lineNickName: UIView!
    @IBOutlet weak var lineAddres: UIView!
    @IBOutlet weak var lineAddressDetail: UIView!
    @IBOutlet weak var lineRecommandCode: UIView!

    @IBOutlet weak var btnMan: UIButton!
    @IBOutlet weak var btnWoman: UIButton!
    
    @IBOutlet weak var viewContainerAddrSearch: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 남성, 여성 버튼
        btnMan.layer.cornerRadius = 16
        btnMan.layer.masksToBounds = true
        btnMan.layer.borderWidth = 1
        btnMan.layer.borderColor = UIColor.init(named: "text_enable_color")?.cgColor
        btnWoman.layer.cornerRadius = 16
        btnWoman.layer.masksToBounds = true
        btnWoman.layer.borderWidth = 1
        btnWoman.layer.borderColor = UIColor.init(named: "text_enable_color")?.cgColor

        // 버튼 상태 설정.
        btnMan.setGenderButtonState(.white, for: .normal)
        btnMan.setGenderButtonState(UIColor.init(named: "text_enable_color")!, for: .selected)
        btnWoman.setGenderButtonState(.white, for: .normal)
        btnWoman.setGenderButtonState(UIColor.init(named: "text_enable_color")!, for: .selected)

        // 주소검색
        viewContainerAddrSearch.layer.cornerRadius = 14
        viewContainerAddrSearch.layer.masksToBounds = true
        viewContainerAddrSearch.layer.borderWidth = 1
        viewContainerAddrSearch.layer.borderColor = UIColor.init(named: "text_enable_color")?.cgColor

        // 기본 남자 선택
        btnMan.isSelected = true
        btnWoman.isSelected = false
    }
    
    
    // MARK: - Method
    
     
    
    // MARK: - IBAction
    
    @IBAction func onNaviBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onMan() {
        btnMan.isSelected = true
        btnWoman.isSelected = false
    }
    
    @IBAction func onWoman() {
        btnMan.isSelected = false
        btnWoman.isSelected = true
    }
    
    @IBAction func onAddrSearch() {
        
    }
    
    @IBAction func onBottomNext() {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "SignUpJobViewController") as! SignUpJobViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField.text != nil else { return true }

        let currentText = textField.text ?? ""
        let inputStr = (currentText as NSString).replacingCharacters(in: range, with: string)
        print("inputStr = \(inputStr)")
        
        // 생년월일 자릿수 제한.
        if textFieldBirthday == textField {
            if inputStr.count > 8 {
                return false
            }
        }
        
        // 전화번호 자릿수 제한.
        if textFieldPhoneNum == textField {
            if inputStr.count > 11 {
                return false
            }
        }

        // 생년월일 8자리 체크
        if textFieldBirthday == textField {
            viewContainerBirthdayError.isHidden = (inputStr.count == 8)
            if viewContainerBirthdayError.isHidden {
                lineBirthday.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                lineBirthday.backgroundColor = UIColor.init(named: "line_error_color")
            }
            
        } else if textFieldPhoneNum == textField {
            viewContainerPhoneNumError.isHidden = Util.shared().isValidPhoneNumber(phone: inputStr)
            if viewContainerPhoneNumError.isHidden {
                linePhoneNum.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                linePhoneNum.backgroundColor = UIColor.init(named: "line_error_color")
            }
            
        } else if textFieldNickName == textField {
            viewContainerNickNameError.isHidden = (inputStr.count == 8)
            if viewContainerNickNameError.isHidden {
                lineNickName.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                lineNickName.backgroundColor = UIColor.init(named: "line_error_color")
            }
            
        } else if textFieldRecommandCode == textField {
            viewContainerRecommCodeError.isHidden = (inputStr.count == 8)
            if viewContainerRecommCodeError.isHidden {
                lineRecommandCode.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                lineRecommandCode.backgroundColor = UIColor.init(named: "line_error_color")
            }
        }
        
        
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textFieldName == textField {
            lineName.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldBirthday == textField {
            lineBirthday.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldPhoneNum == textField {
            linePhoneNum.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldNickName == textField {
            lineNickName.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldAddress == textField {
            lineAddres.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldAddressDetail == textField {
            lineAddressDetail.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldRecommandCode == textField {
            lineRecommandCode.backgroundColor = UIColor.init(named: "line_select_color")
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textFieldName == textField {
            lineName.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldBirthday == textField {
            if textFieldBirthday.text != "" {
                viewContainerBirthdayError.isHidden = (textFieldBirthday.text!.count == 8)
            }
            lineBirthday.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldPhoneNum == textField {
            if textFieldPhoneNum.text != "" {
                viewContainerPhoneNumError.isHidden = Util.shared().isValidPhoneNumber(phone: textField.text!)
            }
            linePhoneNum.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldNickName == textField {
            if textFieldNickName.text != "" {
                viewContainerNickNameError.isHidden = (textFieldNickName.text!.count == 8)
            }
            lineNickName.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldAddress == textField {
            lineAddres.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldAddressDetail == textField {
            lineAddressDetail.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldRecommandCode == textField {
            if textFieldRecommandCode.text != "" {
                viewContainerRecommCodeError.isHidden = (textFieldRecommandCode.text!.count == 8)
            }
            lineRecommandCode.backgroundColor = UIColor.init(named: "line_normal_color")
        }
    }
}
