//
//  SignUpDoneViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/12.
//

// File : SignUpDoneViewController.swift
// Menu : 회원가입 완료 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.12)

import UIKit
import Foundation

class SignUpDoneViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onNaviClose() {
        
    }
}
