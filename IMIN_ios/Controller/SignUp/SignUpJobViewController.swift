//
//  SignUpJobViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/09.
//

// File : SignUpJobViewController.swift
// Menu : 회원가입시 직업 선택 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.9)

import UIKit
import Foundation

class SignUpJobViewController: UIViewController {
    @IBOutlet weak var btnJobType1: UIButton!   // 직장인
    @IBOutlet weak var btnJobType2: UIButton!   // 프리랜서
    @IBOutlet weak var btnJobType3: UIButton!   // 학생
    @IBOutlet weak var btnJobType4: UIButton!   // 주부
    @IBOutlet weak var btnJobType5: UIButton!   // 공무원
    @IBOutlet weak var btnJobType6: UIButton!   // 전문직
    @IBOutlet weak var btnJobType7: UIButton!   // 자영업
    @IBOutlet weak var btnJobType8: UIButton!   // 무직
    @IBOutlet weak var ivPersonImage: UIImageView! // 하단 사람 이미지
    
    var gender: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // 버튼 레이어 설정
        onButtonLayerSetting(sender: btnJobType1)
        onButtonLayerSetting(sender: btnJobType2)
        onButtonLayerSetting(sender: btnJobType3)
        onButtonLayerSetting(sender: btnJobType4)
        onButtonLayerSetting(sender: btnJobType5)
        onButtonLayerSetting(sender: btnJobType6)
        onButtonLayerSetting(sender: btnJobType7)
        onButtonLayerSetting(sender: btnJobType8)

        //  버튼 normal, select 상태 설정
        onButtonStateSetting(sender: btnJobType1)
        onButtonStateSetting(sender: btnJobType2)
        onButtonStateSetting(sender: btnJobType3)
        onButtonStateSetting(sender: btnJobType4)
        onButtonStateSetting(sender: btnJobType5)
        onButtonStateSetting(sender: btnJobType6)
        onButtonStateSetting(sender: btnJobType7)
        onButtonStateSetting(sender: btnJobType8)

        // 디폴트 선택
        onJobSelectEvent(sender: btnJobType1)
        
    }
    
    
    // MARK: - Method
    
    func onButtonLayerSetting(sender: UIButton) {
        sender.layer.cornerRadius = 16
        sender.layer.masksToBounds = true
        sender.layer.borderWidth = 1
    }
    
    func onButtonStateSetting(sender: UIButton) {
        sender.setJobButtonState(state: .normal)
        sender.setJobButtonState(state: .highlighted)
        sender.setJobButtonState(state: .selected)
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onNaviBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onJobSelectEvent(sender: UIButton) {
        btnJobType1.isSelected = false
        btnJobType2.isSelected = false
        btnJobType3.isSelected = false
        btnJobType4.isSelected = false
        btnJobType5.isSelected = false
        btnJobType6.isSelected = false
        btnJobType7.isSelected = false
        btnJobType8.isSelected = false

        btnJobType1.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
        btnJobType2.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
        btnJobType3.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
        btnJobType4.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
        btnJobType5.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
        btnJobType6.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
        btnJobType7.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
        btnJobType8.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor

        // 하단 사람 이미지.
        var gender = DataManager.shared().appSetting.gender // M:남자 , W:여자
        if gender == nil { gender = "M" }
        if gender == "M" { // 남자
            if btnJobType1 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1OfficeM")  } // 직장인
            if btnJobType2 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1FreeM")  } // 프리랜서
            if btnJobType3 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1StudentM")  } // 학생
            if btnJobType4 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1HouseM")  } // 주부
            if btnJobType5 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1PublicM")  } // 공무원
            if btnJobType6 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1ProM")  } // 전문직
            if btnJobType7 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1SelfM")  } // 자영업
            if btnJobType8 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1NoworkM")  } // 무직
        } else { // 여자
            if btnJobType1 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1OfficeW")  } // 직장인
            if btnJobType2 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1FreeW")  } // 프리랜서
            if btnJobType3 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1StudentW")  } // 학생
            if btnJobType4 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1HouseW")  } // 주부
            if btnJobType5 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1PublicW")  } // 공무원
            if btnJobType6 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1ProW")  } // 전문직
            if btnJobType7 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1SelfW")  } // 자영업
            if btnJobType8 == sender {  ivPersonImage.image = UIImage.init(named: "imgFull1NoworkW")  } // 무직
        }
        
        sender.isSelected = true
        sender.layer.borderColor = UIColor.init(named: "text_enable_color")?.cgColor
    }
    
    @IBAction func onBottomNext() {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "SignUpInterestViewController") as! SignUpInterestViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}
