//
//  SignUpAuthViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/08.
//

// File : SignUpAuthViewController.swift
// Menu : 회원가입 인증 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.8)

import UIKit
import Foundation

class SignUpAuthViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldBirthday: UITextField!
    @IBOutlet weak var textFieldPhoneNum: UITextField!

    @IBOutlet weak var viewContainerBirthdayError: UIView!
    @IBOutlet weak var viewContainerPhoneNumError: UIView!

    @IBOutlet weak var lineName: UIView!
    @IBOutlet weak var lineBirthday: UIView!
    @IBOutlet weak var linePhoneNum: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onNaviBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onBottomAuth() {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "SignUpMoreInfoViewController") as! SignUpMoreInfoViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField.text != nil else { return true }

        let currentText = textField.text ?? ""
        let inputStr = (currentText as NSString).replacingCharacters(in: range, with: string)
        print("inputStr = \(inputStr)")
        
        // 생년월일 자릿수 제한.
        if textFieldBirthday == textField {
            if inputStr.count > 8 {
                return false
            }
        }
        
        // 전화번호 자릿수 제한.
        if textFieldPhoneNum == textField {
            if inputStr.count > 11 {
                return false
            }
        }

        // 생년월일 8자리 체크
        if textFieldBirthday == textField {
            viewContainerBirthdayError.isHidden = (inputStr.count == 8)
            if viewContainerBirthdayError.isHidden {
                lineBirthday.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                lineBirthday.backgroundColor = UIColor.init(named: "line_error_color")
            }
        } else if textFieldPhoneNum == textField {
            viewContainerPhoneNumError.isHidden = Util.shared().isValidPhoneNumber(phone: inputStr)
            //setEnableAuthReq(isEnable: viewContainerPhoneNumError.isHidden)
        }
        
        
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textFieldName == textField {
            lineName.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldBirthday == textField {
            lineBirthday.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldPhoneNum == textField {
            linePhoneNum.backgroundColor = UIColor.init(named: "line_select_color")
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textFieldName == textField {
            lineName.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldBirthday == textField {
            if textFieldBirthday.text != "" {
                viewContainerBirthdayError.isHidden = (textFieldBirthday.text!.count == 8)
            }
            lineBirthday.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldPhoneNum == textField {
            if textFieldPhoneNum.text != "" {
                viewContainerPhoneNumError.isHidden = Util.shared().isValidPhoneNumber(phone: textField.text!)
            }
            linePhoneNum.backgroundColor = UIColor.init(named: "line_normal_color")
        }
    }
}
