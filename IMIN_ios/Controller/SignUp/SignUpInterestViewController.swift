//
//  SignUpInterestViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/09.
//

// File : SignUpInterestViewController.swift
// Menu : 회원가입시 관심사나 취미 선택 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.9)

import UIKit
import Foundation

class SignUpInterestViewController: UIViewController {
    @IBOutlet weak var btnInterestType1: UIButton! // 재테크
    @IBOutlet weak var btnInterestType2: UIButton! // 주식
    @IBOutlet weak var btnInterestType3: UIButton! // 요리
    @IBOutlet weak var btnInterestType4: UIButton! // 영화
    @IBOutlet weak var btnInterestType5: UIButton! // 문화/공연
    @IBOutlet weak var btnInterestType6: UIButton! // 게임
    @IBOutlet weak var btnInterestType7: UIButton! // 뷰티
    @IBOutlet weak var btnInterestType8: UIButton! // 여행
    @IBOutlet weak var btnInterestType9: UIButton! // 자동차
    @IBOutlet weak var btnInterestType10: UIButton! // 반려동물
    @IBOutlet weak var btnInterestType11: UIButton! // 쇼핑
    @IBOutlet weak var btnInterestType12: UIButton! // 패션
    @IBOutlet weak var btnInterestType13: UIButton! // 어학
    @IBOutlet weak var btnInterestType14: UIButton! // 스포츠
    @IBOutlet weak var btnInterestType15: UIButton! // 명품
    @IBOutlet weak var btnInterestType16: UIButton! // 전자기기
    @IBOutlet weak var btnInterestType17: UIButton! // 음악
    @IBOutlet weak var btnInterestType18: UIButton! // 독서

    var selectItemCount: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 버튼 레이어 설정
        onButtonLayerSetting(sender: btnInterestType1)
        onButtonLayerSetting(sender: btnInterestType2)
        onButtonLayerSetting(sender: btnInterestType3)
        onButtonLayerSetting(sender: btnInterestType4)
        onButtonLayerSetting(sender: btnInterestType5)
        onButtonLayerSetting(sender: btnInterestType6)
        onButtonLayerSetting(sender: btnInterestType7)
        onButtonLayerSetting(sender: btnInterestType8)
        onButtonLayerSetting(sender: btnInterestType9)
        onButtonLayerSetting(sender: btnInterestType10)
        onButtonLayerSetting(sender: btnInterestType11)
        onButtonLayerSetting(sender: btnInterestType12)
        onButtonLayerSetting(sender: btnInterestType13)
        onButtonLayerSetting(sender: btnInterestType14)
        onButtonLayerSetting(sender: btnInterestType15)
        onButtonLayerSetting(sender: btnInterestType16)
        onButtonLayerSetting(sender: btnInterestType17)
        onButtonLayerSetting(sender: btnInterestType18)

        // normal, select 상태 설정
        btnInterestType1.setInterestButtonState(imgName: "btn_login_money", state: .normal)
        btnInterestType1.setInterestButtonState(imgName: "btn_login_money_select", state: .selected)
        btnInterestType2.setInterestButtonState(imgName: "btn_login_stock", state: .normal)
        btnInterestType2.setInterestButtonState(imgName: "btn_login_stock_select", state: .selected)
        btnInterestType3.setInterestButtonState(imgName: "btn_login_cook", state: .normal)
        btnInterestType3.setInterestButtonState(imgName: "btn_login_cook_select", state: .selected)
        btnInterestType4.setInterestButtonState(imgName: "btn_login_movie", state: .normal)
        btnInterestType4.setInterestButtonState(imgName: "btn_login_movie_select", state: .selected)
        btnInterestType5.setInterestButtonState(imgName: "btn_login_culture", state: .normal)
        btnInterestType5.setInterestButtonState(imgName: "btn_login_culture_select", state: .selected)
        btnInterestType6.setInterestButtonState(imgName: "btn_login_game", state: .normal)
        btnInterestType6.setInterestButtonState(imgName: "btn_login_game_select", state: .selected)
        btnInterestType7.setInterestButtonState(imgName: "btn_login_beauty", state: .normal)
        btnInterestType7.setInterestButtonState(imgName: "btn_login_beauty_select", state: .selected)
        btnInterestType8.setInterestButtonState(imgName: "btn_login_trip", state: .normal)
        btnInterestType8.setInterestButtonState(imgName: "btn_login_trip_select", state: .selected)
        btnInterestType9.setInterestButtonState(imgName: "btn_login_car", state: .normal)
        btnInterestType9.setInterestButtonState(imgName: "btn_login_car_select", state: .selected)
        btnInterestType10.setInterestButtonState(imgName: "btn_login_animal", state: .normal)
        btnInterestType10.setInterestButtonState(imgName: "btn_login_animal_select", state: .selected)
        btnInterestType11.setInterestButtonState(imgName: "btn_login_shopping", state: .normal)
        btnInterestType11.setInterestButtonState(imgName: "btn_login_shopping_select", state: .selected)
        btnInterestType12.setInterestButtonState(imgName: "btn_login_fashion", state: .normal)
        btnInterestType12.setInterestButtonState(imgName: "btn_login_fashion_select", state: .selected)
        btnInterestType13.setInterestButtonState(imgName: "btn_login_language", state: .normal)
        btnInterestType13.setInterestButtonState(imgName: "btn_login_language_select", state: .selected)
        btnInterestType14.setInterestButtonState(imgName: "btn_login_sports", state: .normal)
        btnInterestType14.setInterestButtonState(imgName: "btn_login_sports_select", state: .selected)
        btnInterestType15.setInterestButtonState(imgName: "btn_login_luxury", state: .normal)
        btnInterestType15.setInterestButtonState(imgName: "btn_login_luxury_select", state: .selected)
        btnInterestType16.setInterestButtonState(imgName: "btn_login_electronics", state: .normal)
        btnInterestType16.setInterestButtonState(imgName: "btn_login_electronics_select", state: .selected)
        btnInterestType17.setInterestButtonState(imgName: "btn_login_music", state: .normal)
        btnInterestType17.setInterestButtonState(imgName: "btn_login_music_select", state: .selected)
        btnInterestType18.setInterestButtonState(imgName: "btn_login_book", state: .normal)
        btnInterestType18.setInterestButtonState(imgName: "btn_login_book_select", state: .selected)
    }
    
    
    // MARK: - Method
    
    func onButtonLayerSetting(sender: UIButton) {
        sender.layer.cornerRadius = 16
        sender.layer.masksToBounds = true
        sender.layer.borderWidth = 1
        sender.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onNaviBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onInterestSelectEvent(sender: UIButton) {
        print("selectItemCount = \(selectItemCount)")
        
        // 토글
        if sender.isSelected {
            selectItemCount = selectItemCount - 1
            sender.isSelected = false
            sender.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
        } else {
            // 3개이상 선택되어 있는지 체크.
            if selectItemCount >= 3 {
                let storyboard = UIStoryboard(name: "AlertOkPopup", bundle: nil)
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "AlertOkPopup") as! AlertOkPopup
                popupViewController.centerText = "3개 이상 선택할 수 없습니다."
                popupViewController.onClickOK {
                }
                popupViewController.presentInParentViewController(parentViewController: self)
                return
            }

            selectItemCount = selectItemCount + 1
            if selectItemCount < 0 { selectItemCount = 0 }
            sender.isSelected = true
            sender.layer.borderColor = UIColor.init(named: "text_enable_color")?.cgColor
        }
    }

    @IBAction func onBottomNext() {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "TermOfUseViewController") as! TermOfUseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
