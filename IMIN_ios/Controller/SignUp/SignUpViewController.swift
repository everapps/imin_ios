//
//  SignUpViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/08.
//

// File : TutorialViewController.swift
// Menu : 회원가입 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.8)

import UIKit
import Foundation

class SignUpViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textFieldID: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldPasswordVerify: UITextField!
    
    @IBOutlet weak var viewContainerIDError: UIView!
    @IBOutlet weak var viewContainerPasswordError: UIView!
    @IBOutlet weak var viewContainerPasswordVerifyError: UIView!

    @IBOutlet weak var lineID: UIView!
    @IBOutlet weak var linePassword: UIView!
    @IBOutlet weak var linePasswordVerify: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onNaviBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onBottomNext() {
        //let viewController = self.storyboard!.instantiateViewController(withIdentifier: "SignUpAuthViewController") as! SignUpAuthViewController
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "SignUpMoreInfoViewController") as! SignUpMoreInfoViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField.text != nil else { return true }

        let currentText = textField.text ?? ""
        let inputStr = (currentText as NSString).replacingCharacters(in: range, with: string)
        print("inputStr = \(inputStr)")
        
        if textFieldID == textField {
            viewContainerIDError.isHidden = Util.shared().isValidEmail(email: inputStr)
            if viewContainerIDError.isHidden {
                lineID.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                lineID.backgroundColor = UIColor.init(named: "line_error_color")
            }
        } else if textFieldPassword == textField {
            viewContainerPasswordError.isHidden = Util.shared().isValidPassword(password: inputStr)
            if viewContainerPasswordError.isHidden {
                linePassword.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                linePassword.backgroundColor = UIColor.init(named: "line_error_color")
            }
            
        } else if textFieldPasswordVerify == textField {
            viewContainerPasswordVerifyError.isHidden = (textFieldPassword.text! == inputStr)
            if viewContainerPasswordVerifyError.isHidden {
                linePasswordVerify.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                linePasswordVerify.backgroundColor = UIColor.init(named: "line_error_color")
            }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textFieldID == textField {
            lineID.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldPassword == textField {
            linePassword.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldPasswordVerify == textField {
            linePasswordVerify.backgroundColor = UIColor.init(named: "line_select_color")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textFieldID == textField {
            if textFieldID.text != "" {
                viewContainerIDError.isHidden = Util.shared().isValidEmail(email: textField.text!)
            }
            lineID.backgroundColor = UIColor.init(named: "line_normal_color")
            
        } else if textFieldPassword == textField {
            if textFieldPassword.text != "" {
                viewContainerPasswordError.isHidden = Util.shared().isValidPassword(password: textField.text!)
            }
            linePassword.backgroundColor = UIColor.init(named: "line_normal_color")
            
        } else if textFieldPasswordVerify == textField {
            if textFieldPasswordVerify.text != "" {
                viewContainerPasswordVerifyError.isHidden = (textFieldPassword.text! == textFieldPasswordVerify.text!)
            }
            linePasswordVerify.backgroundColor = UIColor.init(named: "line_normal_color")
        }
    }

}
