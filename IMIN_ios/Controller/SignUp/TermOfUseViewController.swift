//
//  TermOfUseViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/12.
//

// File : TermOfUseViewController.swift
// Menu : 약관동의 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.12)

import UIKit
import Foundation

class TermOfUseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onNaviBack() {
        self.navigationController?.popViewController(animated: true)
    }
}
