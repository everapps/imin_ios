//
//  TutorialViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/02.
//

// File : TutorialViewController.swift
// Menu : 튜토리얼 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.6)

import UIKit
import Foundation
import WebKit

class TutorialViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler {
    @IBOutlet weak var webViewContainer: UIView!
    
    var webView: WKWebView!
    var isIntroUrlStart = false
    
    let toturialURLString = "https://app-api.iminfintech.co.kr/api/v1/stage/tutorial"
    let introURLString = "https://app-api.iminfintech.co.kr/api/v1/intro"

    override func viewDidLoad() {
        super.viewDidLoad()

        let contentController = WKUserContentController()
        contentController.add(self, name: "scriptHandler")
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        config.preferences.javaScriptCanOpenWindowsAutomatically = true
        
        self.webView = WKWebView( frame: self.webViewContainer!.bounds, configuration: config)
        self.webView?.uiDelegate = self
        self.webView?.navigationDelegate = self
        self.webViewContainer.addSubview(self.webView!)
        
        let urlString = toturialURLString
        let url = URL(string: urlString)
        let requestObj = URLRequest(url: url!)
        self.webView?.load(requestObj)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.webView?.frame = self.webViewContainer!.bounds
    }


    // MARK: - IBAction
    
    @IBAction func onNaviBack() {
        self.navigationController?.popViewController(animated: true)
    }

    
    // MARK: 웹뷰관련 - WKScriptMessageHandler
    // WKScriptMessageHandler에 의해 생성된 delegate 함수입니다.
    // 자바스크립트에서 ios에 wekkit핸들러를 통해 postMessage함수를 사용한 경우 실행됩니다.
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if let message = message.body as? String {
            if message == "tutorialComplete" {
                if isIntroUrlStart {
                    // 튜토리얼 show 상태 저장.
                    DataManager.shared().appSetting.isShowTutoral = "Y"
                    DataManager.shared().appSetting.saveData()
                    
                    // 로그인 화면 이동.
                    UIManager.shared().showMainLogin()
                    return
                }
                
                isIntroUrlStart = true
                
                let urlString = introURLString
                let url = URL(string: urlString)
                let requestObj = URLRequest(url: url!)
                self.webView?.load(requestObj)
            }
        }
    }
}
