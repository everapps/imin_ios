//
//  FindIDEndViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/07.
//

// File : FindIDEndViewController.swift
// Menu : 아이디 찾기 완료 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.7)

import UIKit
import Foundation

class FindIDEndViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onLogin() {
        UIManager.shared().showMainLogin()
    }
}
