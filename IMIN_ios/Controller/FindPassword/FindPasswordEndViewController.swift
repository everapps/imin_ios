//
//  FindPasswordEndViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/08.
//

// File : FindPasswordEndViewController.swift
// Menu : 비밀번호 찾기 완료 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.7)

import UIKit
import Foundation

class FindPasswordEndViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldPasswordVerify: UITextField!
    @IBOutlet weak var viewContainerPasswordError: UIView!
    @IBOutlet weak var viewContainerPasswordVerifyError: UIView!

    @IBOutlet weak var linePassword: UIView!
    @IBOutlet weak var linePasswordVerify: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onComplete() {
        UIManager.shared().showMainLogin()
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField.text != nil else { return true }

        let currentText = textField.text ?? ""
        let inputStr = (currentText as NSString).replacingCharacters(in: range, with: string)
        print("inputStr = \(inputStr)")
        
        if textFieldPassword == textField {
            viewContainerPasswordError.isHidden = Util.shared().isValidPassword(password: inputStr)
            if viewContainerPasswordError.isHidden {
                linePassword.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                linePassword.backgroundColor = UIColor.init(named: "line_error_color")
            }
            
        } else if textFieldPasswordVerify == textField {
            viewContainerPasswordVerifyError.isHidden = (textFieldPassword.text! == inputStr)
            if viewContainerPasswordVerifyError.isHidden {
                linePasswordVerify.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                linePasswordVerify.backgroundColor = UIColor.init(named: "line_error_color")
            }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textFieldPassword == textField {
            linePassword.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldPasswordVerify == textField {
            linePasswordVerify.backgroundColor = UIColor.init(named: "line_select_color")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textFieldPassword == textField {
            if textFieldPassword.text != "" {
                viewContainerPasswordError.isHidden = Util.shared().isValidPassword(password: textField.text!)
            }
            linePassword.backgroundColor = UIColor.init(named: "line_normal_color")
        }
        else if textFieldPasswordVerify == textField {
            if textFieldPasswordVerify.text != "" {
                viewContainerPasswordVerifyError.isHidden = (textFieldPassword.text! == textFieldPasswordVerify.text!)
            }
            linePasswordVerify.backgroundColor = UIColor.init(named: "line_normal_color")
        }
    }
}
