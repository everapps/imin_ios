//
//  FindPasswordViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/06.
//

// File : FindPasswordViewController.swift
// Menu : 비밀번호 찾기 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.7)

import UIKit
import Foundation

class FindPasswordViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textFieldID: UITextField!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldPhoneNum: UITextField!
    @IBOutlet weak var textFieldAuthNum: UITextField!
    @IBOutlet weak var viewContainerIDError: UIView!
    @IBOutlet weak var viewContainerPhoneNumError: UIView!
    @IBOutlet weak var viewContainerAuthNumError: UIView!
    @IBOutlet weak var lblRemainTime: UILabel!

    @IBOutlet weak var btnAuthNumReq: UIButton!
    @IBOutlet weak var viewContainerAuthNumReq: UIView!
    @IBOutlet weak var lblAuthNumReq: UILabel!
    
    @IBOutlet weak var btnAuthNumVerify: UIButton!
    @IBOutlet weak var viewContainerAuthNumVerify: UIView!
    @IBOutlet weak var lblAuthNumVerify: UILabel!

    @IBOutlet weak var lineID: UIView!
    @IBOutlet weak var lineName: UIView!
    @IBOutlet weak var linePhoneNum: UIView!
    @IBOutlet weak var lineAuthNum: UIView!

    var timer: Timer? = nil
    let countTime: Int = 180
    var remainCount: Int = 180

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewContainerAuthNumReq.layer.cornerRadius = 14
        viewContainerAuthNumReq.layer.masksToBounds = true
        viewContainerAuthNumReq.layer.borderWidth = 1
        setEnableAuthReq(isEnable: false)
        
        viewContainerAuthNumVerify.layer.cornerRadius = 14
        viewContainerAuthNumVerify.layer.masksToBounds = true
        viewContainerAuthNumVerify.layer.borderWidth = 1
        setEnableAuthVerify(isEnable: false)

    }
    
    // MARK: - Mehtod
    
    // 인증받기/다시받기 버튼 state 관리
    func setEnableAuthReq(isEnable: Bool) {
        btnAuthNumReq.isEnabled = isEnable

        if isEnable {
            viewContainerAuthNumReq.layer.borderColor = UIColor.init(named: "text_enable_color")?.cgColor
            lblAuthNumReq.textColor = UIColor.init(named: "text_enable_color")
        } else {
            viewContainerAuthNumReq.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
            lblAuthNumReq.textColor = UIColor.init(named: "text_disable_color")
        }
    }
    
    // 인증확인 버튼 state 관리
    func setEnableAuthVerify(isEnable: Bool) {
        btnAuthNumVerify.isEnabled = isEnable

        if isEnable {
            viewContainerAuthNumVerify.layer.borderColor = UIColor.init(named: "text_enable_color")?.cgColor
            lblAuthNumVerify.textColor = UIColor.init(named: "text_enable_color")
        } else {
            viewContainerAuthNumVerify.layer.borderColor = UIColor.init(named: "text_disable_color")?.cgColor
            lblAuthNumVerify.textColor = UIColor.init(named: "text_disable_color")
        }
    }
    
    // MARK: - time setting for count timer
    
    func resetCountButton() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        
        lblRemainTime.isHidden = false
        lblRemainTime.text = "3:00"
        remainCount = countTime
        timer = nil
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(startCountTime(_:)), userInfo: nil, repeats: true)
    }
    
    @objc func startCountTime(_ sender: Timer) {
        if remainCount < 1 {
            sender.invalidate()
            lblRemainTime.text = "3:00"

        } else {
            remainCount -= 1
            UIView.performWithoutAnimation {
                lblRemainTime.text = prodTimeString(time: TimeInterval(remainCount))
            }
        }
    }
    
    func prodTimeString(time: TimeInterval) -> String {
        let prodMinutes = Int(time) / 60 % 60
        let prodSeconds = Int(time) % 60

        return String(format: "%d:%02d", prodMinutes, prodSeconds)
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onNaviBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // 인증받기/다시받기
    @IBAction func onAuthNumReq() {
        // 텍스트를 "다시받기" 로 바꾸고 인증번호 입력필드로 포커스 이동.
        lblAuthNumReq.text = "다시받기"
        textFieldAuthNum.becomeFirstResponder()
        
        resetCountButton()
    }

    // 인증확인
    @IBAction func onAuthNumVerify() {
        lblRemainTime.isHidden = true
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        
        viewContainerAuthNumError.isHidden = false
        self.view.endEditing(true)
        
        // 아이디 확인 완료 화면으로 이동.
        let storyboard = UIStoryboard(name: "AlertOkPopup", bundle: nil)
        let popupViewController = storyboard.instantiateViewController(withIdentifier: "AlertOkPopup") as! AlertOkPopup
        popupViewController.centerText = "인증이 완료되었습니다."
        popupViewController.onClickOK {
            let viewController = self.storyboard!.instantiateViewController(withIdentifier: "FindPasswordEndViewController") as! FindPasswordEndViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        popupViewController.presentInParentViewController(parentViewController: self)
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField.text != nil else { return true }

        let currentText = textField.text ?? ""
        let inputStr = (currentText as NSString).replacingCharacters(in: range, with: string)
        print("inputStr = \(inputStr)")
        
        // 전화번호 자릿수 제한.
        if textFieldPhoneNum == textField {
            if inputStr.count > 11 {
                return false
            }
        }
        
        if textFieldID == textField {
            viewContainerIDError.isHidden = Util.shared().isValidEmail(email: inputStr)
            if viewContainerIDError.isHidden {
                lineID.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                lineID.backgroundColor = UIColor.init(named: "line_error_color")
            }
        } else if textFieldPhoneNum == textField {
            viewContainerPhoneNumError.isHidden = Util.shared().isValidPhoneNumber(phone: inputStr)
            setEnableAuthReq(isEnable: viewContainerPhoneNumError.isHidden)
        } else if textFieldAuthNum == textField {
            //viewContainerAuthNumError.isHidden = (inputStr.count == 6)
            setEnableAuthVerify(isEnable: viewContainerAuthNumError.isHidden)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textFieldID == textField {
            lineID.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldName == textField {
            lineName.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldPhoneNum == textField {
            linePhoneNum.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldAuthNum == textField {
            lineAuthNum.backgroundColor = UIColor.init(named: "line_select_color")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textFieldID == textField {
            if textFieldID.text != "" {
                viewContainerIDError.isHidden = Util.shared().isValidEmail(email: textField.text!)
            }
            lineID.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldName == textField {
            lineName.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldPhoneNum == textField {
            if textFieldPhoneNum.text != "" {
                viewContainerPhoneNumError.isHidden = Util.shared().isValidPhoneNumber(phone: textField.text!)
            }
            linePhoneNum.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldAuthNum == textField {
            lineAuthNum.backgroundColor = UIColor.init(named: "line_normal_color")
        }
    }

}
