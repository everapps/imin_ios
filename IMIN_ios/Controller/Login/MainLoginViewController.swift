//
//  MainLoginViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/05.
//

// File : MainLoginViewController.swift
// Menu : 로그인 메인 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.6)

import UIKit
import Foundation

class MainLoginViewController: UIViewController {
    @IBOutlet weak var lblSubjectText: UILabel!
    @IBOutlet weak var viewContainerTakeTour: UIView!
    @IBOutlet weak var viewContainerEmailSignUp: UIView!
    @IBOutlet weak var viewContainerEmailLogin: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 상단 텍스트 어트리뷰트 스트링
        let strSubject: String = "지금 가입하시면\r\n목돈마련지원금\r\n1만원을 드려요."
        let attributedString = NSMutableAttributedString(string: strSubject)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1 // 라인 간격
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        attributedString.setBoldFontColor(color: UIColor.init(named: "text_enable_color")!, forText: "1만원", fontSize: 26.0)
        lblSubjectText.attributedText = attributedString

        // 둘러보기
        viewContainerTakeTour.layer.cornerRadius = 14
        viewContainerTakeTour.layer.masksToBounds = true
        viewContainerTakeTour.layer.borderWidth = 1
        viewContainerTakeTour.layer.borderColor = UIColor.init(rgb: 0xD1D5DC).cgColor

        // 이메일로 가입
        viewContainerEmailSignUp.layer.cornerRadius = 10
        viewContainerEmailSignUp.layer.masksToBounds = true
        
        // 이메일로 로그인
        viewContainerEmailLogin.layer.cornerRadius = 10
        viewContainerEmailLogin.layer.masksToBounds = true
        viewContainerEmailLogin.layer.borderWidth = 1
        viewContainerEmailLogin.layer.borderColor = UIColor.init(named: "text_enable_color")?.cgColor
    }
    
    
    // MARK: - IBAction
    
    // 둘러보기
    @IBAction func onTakeTour() {
        
    }
    
    // 이메일로 회원가입
    @IBAction func onEmailSignUp() {
        let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    // 이메일로 로그인
    @IBAction func onEmailLogin() {
        let storyboard = UIStoryboard(name: "MainLogin", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EmailLoginViewController") as! EmailLoginViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    // 카카오 로그인
    @IBAction func onKakaoLogin() {
        
    }

    // 페이스북 로그인
    @IBAction func onFacebookLogin() {
        
    }

    // 네이버 로그인
    @IBAction func onNaverLogin() {
        
    }


    // 애플 로그인
    @IBAction func onAppleLogin() {
        
    }

}
