//
//  EmailLoginViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/05.
//

// File : EmailLoginViewController.swift
// Menu : 이메일 로그인 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.6)

import UIKit
import Foundation

class EmailLoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textFieldID: UITextField!
    @IBOutlet weak var textFieldPasword: UITextField!
    @IBOutlet weak var viewContainerIDError: UIView!
    @IBOutlet weak var viewContainerPasswordError: UIView!
    @IBOutlet weak var lineID: UIView!
    @IBOutlet weak var linePassword: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touchesBegan!!!")
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onNaviBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // 하단 아이디 찾기
    @IBAction func onFindID() {
        let storyboard = UIStoryboard(name: "FindID", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FindIDViewController") as! FindIDViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // 하단 비밀번호 찾기
    @IBAction func onFindPassword() {
        let storyboard = UIStoryboard(name: "FindPassword", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FindPasswordViewController") as! FindPasswordViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // 로그인
    @IBAction func onLogin() {
        // 이메일이나 비밀번호가 유효한지 다시한번 체크.
//        if !Util.shared().isValidEmail(email: textFieldID.text!) || !Util.shared().isValidPassword(password: textFieldPasword.text!) {
//            return
//        }
        
        // 메인 탭 화면 이동.
        UIManager.shared().showMainTab()
    }
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField.text != nil else { return true }

        let currentText = textField.text ?? ""
        let inputStr = (currentText as NSString).replacingCharacters(in: range, with: string)
        print("inputStr = \(inputStr)")
        
        if textFieldID == textField {
            viewContainerIDError.isHidden = Util.shared().isValidEmail(email: inputStr)
            if viewContainerIDError.isHidden {
                lineID.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                lineID.backgroundColor = UIColor.init(named: "line_error_color")
            }
        } else if textFieldPasword == textField {
            viewContainerPasswordError.isHidden = Util.shared().isValidPassword(password: inputStr)
            if viewContainerPasswordError.isHidden {
                linePassword.backgroundColor = UIColor.init(named: "line_select_color")
            } else {
                linePassword.backgroundColor = UIColor.init(named: "line_error_color")
            }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textFieldID == textField {
            lineID.backgroundColor = UIColor.init(named: "line_select_color")
        } else if textFieldPasword == textField {
            linePassword.backgroundColor = UIColor.init(named: "line_select_color")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textFieldID == textField {
            if textFieldID.text != "" {
                viewContainerIDError.isHidden = Util.shared().isValidEmail(email: textField.text!)
            }
            lineID.backgroundColor = UIColor.init(named: "line_normal_color")
        } else if textFieldPasword == textField {
            if textFieldPasword.text != "" {
                viewContainerPasswordError.isHidden = Util.shared().isValidPassword(password: textField.text!)
            }
            linePassword.backgroundColor = UIColor.init(named: "line_normal_color")
        }
    }
    
}
