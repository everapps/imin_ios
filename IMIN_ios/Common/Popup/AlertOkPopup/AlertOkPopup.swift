//
//  AlertOkPopup.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/07.
//

// File : AlertOkPopup.swift
// Menu : 얼럿 화면
//
// Param :
// 1.centerText - 메시지 내용
//
// History :
// 1. 처음 작성 (2021.7.6)

import UIKit
import Foundation

class AlertOkPopup: PopupBaseViewController {
    @IBOutlet weak var viewPopupBg: UIView!
    @IBOutlet weak var lblCenterText: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    var centerText: String = ""
    var escapedCallback: [() -> ()] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPopupBg.layer.cornerRadius = 10
        viewPopupBg.layer.masksToBounds = true
        
        lblCenterText.text = centerText
    }
    
    
    // MARK: Closure Callback
    
    func onClickOK(okAction: @escaping () -> ()) {
        escapedCallback.append(okAction)
    }
    
    
    // MARK: - IBAction
    
    @IBAction func onBottomOk() {
        escapedCallback[0]()
        dismissFromParentViewController()
    }
    
}
