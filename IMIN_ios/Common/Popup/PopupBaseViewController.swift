//
//  PopupBaseViewController.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/07.
//

// File : PopupBaseViewController.swift
// Menu : 얼럿 화면
//
// Param : 없음
//
// History :
// 1. 처음 작성 (2021.7.5)

import UIKit
import Foundation

class PopupBaseViewController: UIViewController, UIViewControllerTransitioningDelegate {
    var mainWindow: UIWindow = UIApplication.shared.keyWindow!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        modalPresentationStyle = UIModalPresentationStyle.custom
        transitioningDelegate = self
    }
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
    }
    
    func presentInParentViewController(parentViewController: UIViewController) {
        mainWindow.addSubview(self.view)
        parentViewController.addChild(self)
                
        var rect = self.view.bounds
        rect.origin.y = 0
        self.view.frame = rect
        
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
    }
    
    func dismissFromParentViewController() {
        self.willMove(toParent: nil)
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
        
        var rect = self.view.bounds
        rect.origin.y += rect.size.height
        self.view.frame = rect
        
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    
    func presentInParentViewControllerWithAnimate(parentViewController: UIViewController) {
        mainWindow.addSubview(self.view)
        parentViewController.addChild(self)
        
        
        var rect = parentViewController.view.bounds
        rect.origin.y += rect.size.height * 2
        view.frame = rect
        
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut], animations: {
            var rect = self.view.bounds
            rect.origin.y = 0
            self.view.frame = rect
        }, completion: nil)
        
        UIView.animate(withDuration: 0.3, delay: 0.2, options: [], animations: {
            self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            
        }, completion: nil)
        
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
    }
    
    
    func dismissFromParentViewControllerWithAnimate() {
        self.willMove(toParent: nil)
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseOut], animations: {
            var rect = self.view.bounds
            rect.origin.y += rect.size.height
            self.view.frame = rect
            
        }, completion: { _ in
            
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
        
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
}
