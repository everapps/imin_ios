//
//  DataManager.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/02.
//

import Foundation

class DataManager: NSObject {
    
    let appSetting = AppSetting()
    
    struct StaticInstance {
        static var instance: DataManager?
    }
    class func shared() -> DataManager {
        if (StaticInstance.instance == nil) {
            StaticInstance.instance = DataManager()
            initDataManager()
        }
        return StaticInstance.instance!
    }
    
    class func initDataManager() {
    }
    
    func loadData() {
        self.appSetting.loadData()
        
    }
    
    func saveData() {
        self.appSetting.saveData()
    }
}
