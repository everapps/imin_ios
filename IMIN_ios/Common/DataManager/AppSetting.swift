//
//  AppSetting.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/02.
//

import Foundation

class AppSetting: NSObject {
    // 튜토리얼 표시 여부.
    var isShowTutoral: String?
    
    // 회원정보
    var userID: String?     // 유저ID
    var password: String?   // 비밀번호
    var userName: String?   // 유저이름
    var userNickName: String?   // 닉네임
    var gender: String?     // M:남자 , W:여자
    var birthday: String?   // YYYYMMDD
    var phoneNumber: String? // 01011112222
    var address: String?      // 주소
    var addressDetail: String?  // 주소상세
    var jobType: String?    // 직업
    var interest: String?   // 관심사 (쉼표로 구분)
    var isBioAuthYN: String?    // 생체인증 여부

    struct StaticInstance {
        static var instance: AppSetting?
    }
    class func shared() -> AppSetting {
        if (StaticInstance.instance == nil) {
            StaticInstance.instance = AppSetting()
            initAppSetting()
        }
        return StaticInstance.instance!
    }
    
    class func initAppSetting() {
        
    }
    
    func loadData() {
        isShowTutoral = UserDefaults.standard.string(forKey: "isShowTutoral")
        userID = UserDefaults.standard.string(forKey: "userID")
        password = UserDefaults.standard.string(forKey: "password")
        userName = UserDefaults.standard.string(forKey: "userName")
        userNickName = UserDefaults.standard.string(forKey: "userNickName")
        gender = UserDefaults.standard.string(forKey: "gender")
        birthday = UserDefaults.standard.string(forKey: "birthday")
        phoneNumber = UserDefaults.standard.string(forKey: "phoneNumber")
        address = UserDefaults.standard.string(forKey: "address")
        addressDetail = UserDefaults.standard.string(forKey: "addressDetail")
        jobType = UserDefaults.standard.string(forKey: "jobType")
        interest = UserDefaults.standard.string(forKey: "interest")
        isBioAuthYN = UserDefaults.standard.string(forKey: "isBioAuthYN")
    }
       
    func saveData() {
        UserDefaults.standard.set(isShowTutoral, forKey: "isShowTutoral")
        UserDefaults.standard.set(userID, forKey: "userID")
        UserDefaults.standard.set(password, forKey: "password")
        UserDefaults.standard.set(userName, forKey: "userName")
        UserDefaults.standard.set(userNickName, forKey: "userNickName")
        UserDefaults.standard.set(gender, forKey: "gender")
        UserDefaults.standard.set(birthday, forKey: "birthday")
        UserDefaults.standard.set(phoneNumber, forKey: "phoneNumber")
        UserDefaults.standard.set(address, forKey: "address")
        UserDefaults.standard.set(addressDetail, forKey: "addressDetail")
        UserDefaults.standard.set(jobType, forKey: "jobType")
        UserDefaults.standard.set(interest, forKey: "interest")
        UserDefaults.standard.set(isBioAuthYN, forKey: "isBioAuthYN")

        UserDefaults.standard.synchronize()
    }
    
    func removeData() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
}
