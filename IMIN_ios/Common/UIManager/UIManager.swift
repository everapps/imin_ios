//
//  UIManager.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/02.
//

import UIKit
import Foundation

class UIManager: NSObject {
    static var window: UIWindow!
    var mainViewController: MainViewController!
    var mainTabViewController: MainTabViewController!
    
    // MARK: Static Func
    struct StaticInstance {
        static var instance: UIManager?
    }
    class func shared() -> UIManager {
        if (StaticInstance.instance == nil) {
            StaticInstance.instance = UIManager()
            initUIManager()
        }
        return StaticInstance.instance!
    }
    
    class func initUIManager() {
        window = UIWindow.init(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.init(named: "theme_color")
        window.makeKeyAndVisible()
    }
    
//    func showIndicator(view: UIView) {
//        DispatchQueue.main.async {
//            if self.hud != nil {
//                self.hud.hide(animated: true)
//            }
//            self.hud = MBProgressHUD.showAdded(to: view, animated: true)
//        }
//    }
//
//    func hideIndicator(view: UIView) {
//        DispatchQueue.main.async {
//            self.hud.hide(animated: true)
//        }
//    }

    
    // MARK: Method
    
    func showMain() {
        let storyboard: UIStoryboard = (UIStoryboard(name: "Main", bundle: nil) as UIStoryboard?)!
        mainViewController = (storyboard.instantiateInitialViewController() as! MainViewController)
        UIManager.window!.rootViewController = self.mainViewController
    }
        
    func showIntro() {
        let storyboard: UIStoryboard = (UIStoryboard(name: "Intro", bundle: nil) as UIStoryboard?)!
        let naviController : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        mainViewController.setMainContentViewController(naviController)
    }
    
    func showTutorial() {
        let storyboard: UIStoryboard = (UIStoryboard(name: "Tutorial", bundle: nil) as UIStoryboard?)!
        let naviController : UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        mainViewController.setMainContentViewController(naviController)
    }

    func showMainLogin() {
        let storyboard: UIStoryboard = (UIStoryboard(name: "MainLogin", bundle: nil) as UIStoryboard?)!
        let naviController: UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        mainViewController.setMainContentViewController(naviController)
    }

    func showMainTab() {
        let storyboard: UIStoryboard = (UIStoryboard(name: "MainTab", bundle: nil) as UIStoryboard?)!
        let naviController: UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        self.mainTabViewController = (naviController.viewControllers.first as! MainTabViewController)
        mainViewController.setMainContentViewController(naviController)
    }
}

