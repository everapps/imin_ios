//
//  AppDelegate.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/02.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // 키보드 관련
        IQKeyboardManager.shared.placeholderColor = UIColor.clear
        IQKeyboardManager.shared.enable = true

        // 저장 데이타 로드.
        DataManager.shared().appSetting.loadData()
        
        // UI
        DispatchQueue.main.async {
            UIManager.shared().showMain()
            UIManager.shared().showIntro()
        };

        return true
    }
}

