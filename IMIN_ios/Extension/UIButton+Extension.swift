//
//  UIButton+Extension.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/08.
//

import UIKit

extension UIButton {
    // 남/여 선택 버튼
    func setGenderButtonState(_ color: UIColor, for state: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1.0, height: 1.0))
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setFillColor(color.cgColor)
        context.fill(CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0))
        
        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
         
        self.setBackgroundImage(backgroundImage, for: state)
        
        if state == .normal {
            setTitleColor(UIColor.init(named: "text_enable_color")!, for: .normal)
        } else if state == .selected {
            setTitleColor(UIColor.white, for: .selected)
        }
    }
    
    // 직업 선택 버튼
    func setJobButtonState(state: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1.0, height: 1.0))
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setFillColor(UIColor.white.cgColor)
        context.fill(CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0))

        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        self.setBackgroundImage(backgroundImage, for: state)

        if state == .normal {
            setTitleColor(UIColor.init(named: "text_normal_color"), for: .normal)
        } else if state == .highlighted {
            setTitleColor(UIColor.init(named: "text_normal_color"), for: .highlighted)
        } else if state == .selected {
            setTitleColor(UIColor.init(named: "text_enable_color"), for: .selected)
        }
    }
    
    // 관심사 선택 버튼
    func setInterestButtonState(imgName: String, state: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1.0, height: 1.0))
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setFillColor(UIColor.white.cgColor)
        context.fill(CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0))

        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        self.setBackgroundImage(backgroundImage, for: state)

        if state == .normal {
            setImage(UIImage.init(named: imgName), for: .normal)
            setTitleColor(UIColor.init(named: "text_normal_color"), for: .normal)
        } else if state == .selected {
            setImage(UIImage.init(named: imgName), for: .selected)
            setTitleColor(UIColor.init(named: "text_enable_color"), for: .selected)
        }
    }
}


