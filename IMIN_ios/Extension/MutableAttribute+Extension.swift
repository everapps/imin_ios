//
//  MutableAttribute+Extension.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/05.
//

import UIKit

extension NSMutableAttributedString {
    func setColor(color: UIColor, forText stringValue: String) {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }

    func setBoldFontColor(color: UIColor, forText stringValue: String, fontSize: CGFloat) {
        let fontType = UIFont.init(name: "AppleSDGothicNeo-Bold", size: fontSize)!
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        self.addAttribute(NSAttributedString.Key.font, value: fontType, range: range)
    }

    func setRegularFontColor(color: UIColor, fontSize: CGFloat, forText stringValue: String) {
        let fontType = UIFont(name: "AppleSDGothicNeo-Regular", size: fontSize)!
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        self.addAttribute(NSAttributedString.Key.font, value: fontType, range: range)
    }
}
