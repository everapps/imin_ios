//
//  Util.swift
//  IMIN_ios
//
//  Created by sjjung_MBPR on 2021/07/07.
//

import UIKit
import Foundation

@objc class Util: NSObject {
    struct StaticInstance {
        static var instance: Util?
    }
    
    class func shared() -> Util {
        if (StaticInstance.instance == nil) {
            StaticInstance.instance = Util()
        }
        return StaticInstance.instance!
    }
    
    // 이메일 유효성 체크
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    // 비밀번호 유효성 체크
    func isValidPassword(password: String) -> Bool {
        let pwRegex = "^(?=.*[a-z])(?=.*\\d)[a-zA-Z\\d]{8,}$" // 8자리 이상, 영문대소문자/숫자
        let pwTest = NSPredicate(format: "SELF MATCHES %@", pwRegex)
        return pwTest.evaluate(with: password)
    }

    // 핸드폰번호 유효성 체크
    func isValidPhoneNumber(phone: String) -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{9,11}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: phone)
    }

}
